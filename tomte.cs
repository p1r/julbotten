using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Constant = StringConstant.cs.StringConstant;

namespace tomten
{
    class Program
    {
        async static Task Main(string[] args)
        {
            var tomtes = new List<Tomte>()
            {
                new Tomte{
                    Name = "Exempel",
                    Number = "+555555"
                }
            };

            for (var i = tomtes.Count; i > 0; i--)
            {
                var aspiringGifters = tomtes.Where(x => x.GivesTo == null).ToList();
                var aspiringReceivers = tomtes.Where(x => x.ReceivesFrom == null).ToList();

                if (aspiringGifters.Count == 3 && aspiringReceivers.Count == 3)
                {
                    for (var j = 0; j < 3; j++)
                    {
                        if (j <= 1)
                        {
                            aspiringGifters[j].GivesTo = aspiringReceivers[j + 1];
                            aspiringReceivers[j + 1].ReceivesFrom = aspiringGifters[j];
                        }
                        else
                        {
                            aspiringGifters[j].GivesTo = aspiringReceivers[0];
                            aspiringReceivers[0].ReceivesFrom = aspiringGifters[j];
                        }
                    }

                    break;
                }

                Tomte gifter = null;
                Tomte receiver = null;

                while (gifter == receiver)
                {
                    var result = Scramble(aspiringGifters, aspiringReceivers);
                    gifter = result.Gifter;
                    receiver = result.Receiver;
                }

                gifter.GivesTo = receiver;
                receiver.ReceivesFrom = gifter;
            }

            if (tomtes.Any(x => x == x.GivesTo || x == x.ReceivesFrom))
            {
                Console.WriteLine("nOOOO");
                throw new Exception();
            }

            var facit = "";
            foreach (var tomte in tomtes)
            {
                facit +=
                    $"{tomte.Name} gives to {tomte.GivesTo?.Name} and receives from {tomte.ReceivesFrom?.Name}{Environment.NewLine}";

                
                var mess = string.Format(Constant.Message, tomte.Name, tomte.GivesTo.Name.ToUpper());
                await SendSms(tomte.Name, tomte.Number, mess);
            }

            var message = Constant.MessageToJudge + facit;

            // send cheats to Judge
            await SendSms("Judge", "+666", message);
        }


        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        private static (Tomte Gifter, Tomte Receiver) Scramble(List<Tomte> aspiringGifters,
            List<Tomte> aspiringReceivers)
        {
            var rand = new Random();
            return (aspiringGifters[rand.Next(0, aspiringGifters.Count - 1)],
                aspiringReceivers[rand.Next(0, aspiringGifters.Count - 1)]);
        }

        public static async Task SendSms(string name, string number, string message)
        {
            var user = "";
            var pwd = "";

            using var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", Base64Encode($"{user}:{pwd}"));

            client.BaseAddress = new Uri("https://api.46elks.com/");

            var request = new HttpRequestMessage(HttpMethod.Post, "a1/sms")
            {
                Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("from", Constant.BotName),
                    new KeyValuePair<string, string>("to", number),
                    new KeyValuePair<string, string>("message", message)
                })
            };
            try
            {
                var response = await client.SendAsync(request);

                if (!response.IsSuccessStatusCode)
                    Console.WriteLine(string.Format(Constant.FailMessage, name, response.StatusCode,
                        response.ReasonPhrase));
                else
                    Console.WriteLine(string.Format(Constant.SuccessMessage, name, response.StatusCode));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{name} fick inget SMS!");
                Console.WriteLine(ex);
            }
        }

        public class Tomte
        {
            public string Name { get; set; }
            public string Number { get; set; }
            public Tomte GivesTo { get; set; }
            public Tomte ReceivesFrom { get; set; }
        }
    }
}