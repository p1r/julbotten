﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Net.Http;
// using System.Net.Http.Headers;
// using System.Threading.Tasks;
// using Constant = StringConstant.cs.StringConstant;

// namespace tomten
// {
//     class Program
//     {
//         static void Main(string[] args)
//         {
//             var tomtes = new List<Tomte>()
//             {
//                 new Tomte()
//                 {
//                     Name = "Petter",
//                     Number = 17171,
//                 },
//                 new Tomte()
//                 {
//                     Name = "Olof",
//                     Number = 1717
//                 },
//                 new Tomte()
//                 {
//                     Name = "Kerstin",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Lars-Gunnar",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Gunnel",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Ania",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Hans",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Phoebe",
//                     Number = 1337
//                 },
//                 new Tomte()
//                 {
//                     Name = "Metta",
//                     Number = 1337
//                 }
//             };

//             for (var i = tomtes.Count; i > 0; i--)
//             {
//                 var aspiringGifters = tomtes.Where(x => x.GivesTo == null).ToList();
//                 var aspiringReceivers = tomtes.Where(x => x.ReceivesFrom == null).ToList();

//                 if (aspiringGifters.Count == 3 && aspiringReceivers.Count == 3)
//                 {
//                     for (var j = 0; j < 3; j++)
//                     {
//                         if (j <= 1)
//                         {
//                             aspiringGifters[j].GivesTo = aspiringReceivers[j + 1];
//                             aspiringReceivers[j + 1].ReceivesFrom = aspiringGifters[j];
//                         }
//                         else
//                         {
//                             aspiringGifters[j].GivesTo = aspiringReceivers[0];
//                             aspiringReceivers[0].ReceivesFrom = aspiringGifters[j];
//                         }
//                     }
//                     break;
//                 }

//                 Tomte gifter = null;
//                 Tomte receiver = null;

//                 while (gifter == receiver)
//                 {
//                     var result = Scramble(aspiringGifters, aspiringReceivers);
//                     gifter = result.Gifter;
//                     receiver = result.Receiver;
//                 }
               
//                 gifter.GivesTo = receiver;
//                 receiver.ReceivesFrom = gifter;
//             }

//             foreach (var tomte in tomtes)
//             {
//                 // SendSms(tomte);
//                 Console.WriteLine(
//                     $"{tomte.Name} gives to {tomte.GivesTo?.Name} and receives from {tomte.ReceivesFrom?.Name}");
//             }

//             // Console.WriteLine(string.Format(message, "Petter", "Olof".ToUpper()));
//             // Console.ReadLine();
//         }

//         private static (Tomte Gifter, Tomte Receiver) Scramble(List<Tomte> aspiringGifters, List<Tomte> aspiringReceivers)
//         {
//             var rand = new Random();
//             return (aspiringGifters[rand.Next(0, aspiringGifters.Count - 1)], aspiringReceivers[rand.Next(0, aspiringGifters.Count - 1)]);
//         }

//         public static async Task SendSms(Tomte tomte)
//         {
//             var user = "<API Username>";
//             var pwd = "<API Password>";

//             // string.Format(message, tomte.Name, tomte.GivesTo.Name.ToUpper());
//             using var client = new HttpClient { BaseAddress = new Uri("https://api.46elks.com") };

//             client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
//                 "Basic",
//                 Convert.ToBase64String(
//                     System.Text.Encoding.ASCII.GetBytes(
//                         $"{user}:{pwd}")));

//             var content = new FormUrlEncodedContent(new[]
//             {
//                 new KeyValuePair<string, string>("from", Constant.BotName),
//                 new KeyValuePair<string, string>("to", tomte.Number.ToString()),
//                 new KeyValuePair<string, string>("message",
//                     string.Format(Constant.Message, tomte.Name, tomte.GivesTo.Name.ToUpper()))
//             });

//             var response = await client.PostAsync("/a1/SMS", content);
           
//             if (!response.IsSuccessStatusCode)
//                 Console.WriteLine(string.Format(Constant.FailMessage, tomte.Name, response.StatusCode, response.ReasonPhrase));
//             else
//                 Console.WriteLine(string.Format(Constant.SuccessMessage, tomte.Name, response.StatusCode));

//             var result = await response.Content.ReadAsStringAsync();
//             Console.WriteLine(result);
//         }
//         // public async Task SendSms()
//         // {
//         //     var user = "<API Username>";
//         //     var pwd = "<API Password>";
//         //
//         //     using var client = new HttpClient { BaseAddress = new Uri("https://api.46elks.com") };
//         //
//         //     client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
//         //         "Basic",
//         //         Convert.ToBase64String(
//         //             System.Text.Encoding.ASCII.GetBytes(
//         //                 $"{user}:{pwd}")));
//         //
//         //     var content = new FormUrlEncodedContent(new[]
//         //     {
//         //         new KeyValuePair<string, string>("from", "JULBOTTEN"),
//         //         new KeyValuePair<string, string>("to", "+46735417172"),
//         //         new KeyValuePair<string, string>("message", "Test 😊")
//         //     });
//         //
//         //     var response = await client.PostAsync("/a1/SMS", content);
//         //     response.EnsureSuccessStatusCode();
//         //     var result = await response.Content.ReadAsStringAsync();
//         // }

//         public class Tomte
//         {
//             public string Name { get; set; }
//             public int Number { get; set; }
//             public Tomte GivesTo { get; set; }
//             public Tomte ReceivesFrom { get; set; }
//         }
//     }
// }