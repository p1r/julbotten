using System;

namespace StringConstant.cs
{
    public static class StringConstant
    {
        public static readonly string BotName = "Julbotten";
        
        public static string Message = "Hej {0}!" +
                      $"{Environment.NewLine}" +
                      $"{Environment.NewLine}" +
                      "Välkommen att fira jul i Uppsala!" +
                      $"{Environment.NewLine}" +
                      "Du ska ge en julklapp till *trumvirvel* {1} !! 🎁🎅" +
                      $"{Environment.NewLine}" +
                      $"(Den här informationen är hemlig! 🤐)" +
                      $"{Environment.NewLine}" +
                      $"{Environment.NewLine}" +
                      $"Tack för att du deltar i Eksoppsvägens automatiserade julklappsystem! 🙏" +
                      $"{Environment.NewLine}" +
                      $"{Environment.NewLine}" +
                      $"Vid eventuella frågor så hör av er till Olof." +
                      $"{Environment.NewLine}" +
                      $"{Environment.NewLine}" +
                      $"God Jul önskar {BotName}! 🎄";

        public static string MessageToJudge =
            $"Tjänna Jojjeee!{Environment.NewLine}" +
            $"Här kommer facit från min skapare och mästare som önskar dig en god jul. 🎄{Environment.NewLine}" +
            $"Dubbelchecka gärna så att allt stämmer :) {Environment.NewLine}{Environment.NewLine}";
        public static string FailMessage = "oOOps det gick inte att skicka till {0}, statuskod: {0} anledning {2}";
        public static string SuccessMessage = "Skickade SMS till {0} {1}";
        
    }
}